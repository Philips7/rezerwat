import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Recommendation from './Recommendation'
import {clutch, flymble} from './clutch'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Recommendation info={flymble} />, document.getElementById('root'));
registerServiceWorker();
