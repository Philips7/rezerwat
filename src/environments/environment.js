import firebase from './firebase'

const env = {
    firebase
}

export default env
