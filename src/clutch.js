export const clutch = {
    maxRating: 5.0,
    rating: 4.2,

}

export const freight = {
    rating: 5.0,
    quality: 5.0,
    schedule: 5.0,
    cost: 5.0,
    willingToRefer: 5.0,
    id: 'review-223790'
}

export const flymble = {
    rating: 5.0,
    quality: 5.0,
    schedule: 5.0,
    cost: 5.0,
    willingToRefer: 5.0,
    id: 'review-224996'
}

export const sportyHQ = {
    rating: 5.0,
    quality: 5.0,
    schedule: 5.0,
    cost: 5.0,
    willingToRefer: 5.0,
    id: 'review-225128'
}

export const ecommerceSite = {
    rating: 4.5,
    quality: 4.5,
    schedule: 3.5,
    cost: 4.5,
    willingToRefer: 5.0,
    id: 'review-240597'
}

export const softwareFirm = {
    rating: 5.0,
    quality: 4.5,
    schedule: 4.5,
    cost: 3.5,
    willingToRefer: 4.5,
    id: 'review-262203'
}

export const creativeAgency = {
    rating: 4.5,
    quality: 5.0,
    schedule: 4.5,
    cost: 4.5,
    willingToRefer: 5.0,
    id: 'review-290249'
}
