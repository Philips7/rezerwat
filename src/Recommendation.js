import React from 'react'
import classNames from 'classnames'

import Stars from './Stars'
import c from './recommendation.scss'

const width = 21
const height = 21

const Recommendation = ({info : {quality, rating, schedule, cost, willingToRefer}}) => (
    <div className={c.container}>
        <div className={c.rating}>
            <div className={c.info}>
                <div className={c.item}>
                    <Stars width={width} height={height}/>
                </div>
                <p className={c.item}>Quality: <b>{quality.toFixed(1)}</b></p>
                <p className={c.item}>Schedule: <b>{schedule.toFixed(1)}</b></p>
            </div>
            <div className={c.info}>
                <p className={c.item}><b>Rated {rating.toFixed(1)} / 5.0 ON CLUTCH</b></p>
                <p className={c.item}>Cost: <b>{cost.toFixed(1)}</b></p>
                <p className={c.item}>Willing To Refer: <b>{willingToRefer.toFixed(1)}</b></p>
            </div>
        </div>
        <a href="https://clutch.co/profile/7ninjas#review-225128">
            <button type="button">
                SEE ON CLUTCH
            </button>
        </a>
    </div>
)

export default Recommendation