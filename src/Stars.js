import React from 'react';

const starsCount = 5
const rating = 4.2

const FullStar = ({width, height}) => (
    <svg
        xmlns='http://www.w3.org/2000/svg'
        xmlnsXlink='http://www.w3.org/1999/xlink'
        width={width}
        height={height}
        viewBox='0 0 13 13'>
        <defs>
            <path id='isf3a'
                  d='M648 7750.8c0 .12-.07.24-.2.38l-2.84 2.77.67 3.9.01.16c0 .1-.03.2-.08.28a.28.28 0 0 1-.24.1c-.1 0-.2-.02-.31-.09l-3.51-1.84-3.5 1.84a.66.66 0 0 1-.32.1c-.11 0-.2-.04-.25-.11a.46.46 0 0 1-.08-.28c0-.03 0-.09.02-.16l.67-3.9-2.84-2.77c-.13-.14-.2-.27-.2-.38 0-.19.15-.3.44-.35l3.92-.57 1.76-3.56c.1-.21.22-.32.38-.32s.28.1.38.32l1.76 3.56 3.92.57c.3.04.44.16.44.35z'
            />
        </defs>
        <use fill='#ff9500' xlinkHref='#isf3a' transform='translate(-635 -7746)'
        />
    </svg>
)

const HalfStar = ({width, height}) => (
    <svg xmlns='http://www.w3.org/2000/svg' xmlnsXlink='http://www.w3.org/1999/xlink'
         width={width} height={height} viewBox='0 0 21 20'>
        <defs>
            <path id='pkita'
                  d='M272.96 7184.93l.15.81.73 4.36-3.9-2.06-.73-.38v-11.82l1.96 3.95.36.74.81.12 4.38.64-3.16 3.07zm6.44-4.52c-.07-.23-.3-.37-.66-.42l-6.16-.9-2.76-5.59c-.18-.33-.38-.5-.6-.5-.24 0-.44.17-.6.5l-2.77 5.59-6.16.9c-.37.05-.6.2-.67.42-.07.22.02.47.29.73l4.47 4.35-1.06 6.13c-.04.27-.02.48.07.64.1.15.23.22.42.22.14 0 .3-.05.5-.14l5.5-2.9 5.52 2.9c.18.1.35.14.49.14.19 0 .32-.07.41-.22.1-.16.12-.37.08-.64l-1.06-6.13 4.46-4.35c.27-.26.37-.5.3-.73z'
            />
        </defs>
        <use fill='#ff9500' xlinkHref='#pkita' transform='translate(-259 -7173)'/>
    </svg>
)

const Stars = ({width, height}) => (
    <span>
        {[...Array(starsCount)].map((x, i) =>
            <span key={i}>
                {(rating - i > 0.5) &&
                    <FullStar width={width} height={height} />
                }

                {((Math.abs(rating - i) <= 0.5) && rating - i > 0) &&
                    <HalfStar width={width} height={height}/>
                }
            </span>
        )}
    </span>
)

export default Stars